# DevOps Continuous Integration & Delivery pipeline

This repository serves as personal portfolio in which my knowledge about DevOps tools is used to create a fully functional pipeline.

## Tools and technologies used:
![DevOps technologies](./desc-images/DevOps.png "DevOps technologies")

- Bash scripting
- Docker (Docker-compose)
- Git (Integrations)
- Jenkins (CI/CD, Shared Library)
- Groovy
- Python (Boto3)
- AWS (VPC, EC2, ELB, ECR, CloudWatch, ASG, EKS, IAM)
- Ansible
- Terraform
- K8S (Service, Deployment, PVC)
- Helm chart
- Prometheus & Grafana

## Project plan:
![Project Plan](./desc-images/ProjectPlan.jpg "Project Plan")

1. Create repository on Gitlab with the application source code
2. Set up *Jenkins* server on *AWS EC2* instance using *Terraform* [infrastructure files](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/tree/master/jenkins-setup/jenkins-terraform) and *Ansible* [playbook](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/tree/master/jenkins-setup/deployment-files)
3. Create a multibranch pipeline job on *Jenkins* and install [necessary plugins](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/blob/master/jenkins-plugins-used.txt)
4. Create the [Jenkinsfile](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/blob/master/Jenkinsfile) with following steps:
- Use [Dockerfile](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/blob/master/Dockerfile) to build the image
- Push created application image to Amazon ECR
- Set up infrastructure for EKS using Terraform [files](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/tree/master/eks-terraform-files)
- Create deployment files, pull application image and deploy to EKS pods
5. Use *GitLab integration* web hooks to automate pipeline triggers
6. Using *Terraform* [files](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/tree/master/monitoring-setup) create new *EC2 instance* for monitoring
7. Install necessary packages on newly created instance using Ansible [playbook](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/blob/master/monitoring-setup/package-installation/package-install.yaml)
8. Set up *Prometheus server* on EC2 instance using *Helm charts* and [scripting](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/blob/master/monitoring-setup/package-installation/monitoring.sh)
9. Set up *Grafana* on top of Prometheus server
10. Write [scripts](https://gitlab.com/javidjhuseyn/devops-full-pipeline/-/tree/master/python-aws-actions) using *Python* and *Boto3* module to make use of AWS API

## Author Info:
Javid Huseyn, MSc Software of Computer Networks and Systems
