terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws-region
}

data "aws_ami" "jenkins-ami" {
  most_recent = true

  filter {
    name 	= "name"
    values 	= [var.ami-name]
  }

  filter {
    name	= "virtualization-type"
    values 	= ["hvm"]
  }
 
  owners = [var.ami-owner]
}

resource "aws_instance" "jenkins-server" {
  ami = data.aws_ami.jenkins-ami.id
  instance_type	= var.aws-instance-type
  key_name = "jenkins-server-key"

  subnet_id = aws_subnet.jenkins-public-subnet.id
  security_groups = [aws_security_group.jenkins-security-group.id]

  tags = {
    Name = "jenkins-server"
  }
}

resource "aws_key_pair" "jenkins-server-key" {
  key_name    = "jenkins-server-key"
  public_key  = var.jenkins-server-key
}

