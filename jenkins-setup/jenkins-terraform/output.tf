output "jenkins-server-public-address" {
    value = aws_instance.jenkins-server.public_ip
}