resource "aws_vpc" "jenkins-vpc" {
  cidr_block = var.vpc-cidr-block

  tags = {
    Name = "jenkins-vpc"
  }
}

resource "aws_subnet" "jenkins-public-subnet" {
  cidr_block = var.public-subnet-cidr-block
  vpc_id = aws_vpc.jenkins-vpc.id
  availability_zone = var.az-jenkins
  map_public_ip_on_launch = true

  tags = {
    Name = "jenkins-public-subnet"
  }
}

resource "aws_subnet" "jenkins-private-subnet"{
  cidr_block = var.private-subnet-cidr-block
  vpc_id = aws_vpc.jenkins-vpc.id
  availability_zone = var.az-jenkins

  tags = {
    Name = "jenkins-private-subnet"
  }
}

resource "aws_security_group" "jenkins-security-group" {
  vpc_id = aws_vpc.jenkins-vpc.id

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH for VPC"
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP for Jenkins"
    from_port = 8888
    protocol = "tcp"
    to_port = 8888
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jenkins-sg"
  }
}

resource "aws_internet_gateway" "jenkins-internet-gateway" {
  vpc_id = aws_vpc.jenkins-vpc.id
}

resource "aws_route_table" "jenkins-route-table" {
  vpc_id = aws_vpc.jenkins-vpc.id
  route {
    cidr_block = var.route-table-cidr-block
    gateway_id = aws_internet_gateway.jenkins-internet-gateway.id
  }
}

resource "aws_route_table_association" "jenkins-route-table-association" {
  route_table_id = aws_route_table.jenkins-route-table.id
  subnet_id = aws_subnet.jenkins-public-subnet.id
}
