resource "kubernetes_deployment" "node-db" {
  metadata {
    name = var.db_instance_name
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.db_instance_name
      }
    }
    template {
      metadata {
        labels = {
          app = var.db_instance_name
        }
      }
      spec {
        container {
          image = "mongo"
          name  = var.db_instance_name
          port {
            container_port = 27017
          }
          env {
            name = "MONGO_INITDB_ROOT_USERNAME"
            value = var.mongo_root_username
          }
          env {
            name = "MONGO_INITDB_ROOT_PASSWORD"
            value = var.mongo_root_password
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "node-app" {
  metadata {
    name = var.app_instance_name
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.app_instance_name
      }
    }
    template {
      metadata {
        labels = {
          app = var.app_instance_name
        }
      }
      spec {
        container {
          image = var.image_name
          name  = var.app_instance_name
          port {
            container_port = 3000
          }
          env {
            name = "MONGO_ROOT_USERNAME"
            value = var.mongo_root_username
          }
          env {
            name = "MONGO_ROOT_PASSWORD"
            value = var.mongo_root_password
          }
          env {
            name = "MONGO_URL"
            value = var.mongodb_url
          }   
        }
      }
    }
  }
  depends_on = [
    kubernetes_deployment.node-db,
    kubernetes_service.node-db
  ]
}

