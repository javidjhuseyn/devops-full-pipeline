data "aws_eks_cluster" "eks-cluster" {
    name = module.eks-cluster.cluster_id
}

data "aws_eks_cluster_auth" "eks-cluster-auth" {
    name = module.eks-cluster.cluster_id
} 

module "eks-cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"

  cluster_name = "eks-cluster"  
  cluster_version = "1.21"

  subnets = module.eks-vpc.private_subnets
  vpc_id = module.eks-vpc.vpc_id

  tags = {
    environment = "development"
    application = "app"
  }

  node_groups = {
    first = {
      desired_capacity = 2
      max_capacity     = 3
      min_capacity     = 1

      instance_type = "t3.medium"
    }
  }
  write_kubeconfig   = true
}
