output "eks-endpoint" {
  value = data.aws_eks_cluster.eks-cluster.endpoint
}
output "node-app-load-balancer_hostname" {
  value = kubernetes_service.node-app.status.0.load_balancer.0.ingress.0.hostname
}