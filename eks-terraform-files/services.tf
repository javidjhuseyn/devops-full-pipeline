resource "kubernetes_service" "node-db" {
    metadata {
        name = "node-db"
    }
    spec {
        selector = {
            app = "node-db"
        }
        port {
            protocol = "TCP"
            port = 27017
            target_port = 27017
        }
    }
}

resource "kubernetes_service" "node-app" {
    metadata {
        name = "node-app"
    }

    spec {
        selector = {
            app = "node-app"
        }
        port {
            protocol = "TCP"
            port = 80
            target_port = 3000
        }
        type = "LoadBalancer"
    } 
}