variable aws_region {
    default = "eu-west-2"
    type = string
}
variable vpc_cidr_block {
    default = "10.0.0.0/16"
    type = string
}
variable private_subnet_cidr_blocks {
    default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
    type = list(string)
}
variable public_subnet_cidr_blocks {
    default = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
    type = list(string)
}
variable image_name {
    default = "844963371023.dkr.ecr.eu-west-3.amazonaws.com/devops-pipeline:latest"
    type = string
}
variable db_instance_name {
    default = "node-db"
    type = string
}
variable app_instance_name {
    default = "node-app"
    type = string
}
variable mongo_root_username {
    default = "admin"
    type = string
    sensitive = true
}
variable mongo_root_password {
    default = "password"
    type = string
    sensitive = true
}
variable mongodb_url {
    default = "mongodb://mongo:27017/dev"
    type = string
}