def buildImage(){
    echo 'Building the Docker image ...'
    sh "docker build -t ${ECR_REPO}:latest ."
}

def pushToECR(){
    docker.withRegistry("https://${ECR_REPO}", "ecr:${AWS_REGION}:aws-credentials") {
    docker.image("${ECR_REPO}:latest").push()
    }
}

def configureEKSCluster(){
    dir('eks-terraform-files') {
        withCredentials([[$class: 'AmazonWebServicesCredentialsBinding',credentialsId: "aws-credentials",accessKeyVariable: 'AWS_ACCESS_KEY_ID',secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            sh 'terraform fmt'
            sh 'terraform init'
            sh 'terraform plan'
            // sh 'terraform apply --auto-approve'
            sh 'terraform destroy --auto-approve'
        }
    }
}

return this