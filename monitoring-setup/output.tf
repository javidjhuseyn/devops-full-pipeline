output "prometheus-server-public-address" {
    value = aws_instance.prometheus-server.public_ip
}