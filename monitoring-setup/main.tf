terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws-region
}

data "aws_ami" "prometheus-ami" {
  most_recent = true

  filter {
    name 	= "name"
    values 	= [var.ami-name]
  }

  filter {
    name	= "virtualization-type"
    values 	= ["hvm"]
  }
 
  owners = [var.ami-owner]
}

resource "aws_instance" "prometheus-server" {
  ami = data.aws_ami.prometheus-ami.id
  instance_type	= var.aws-instance-type
  key_name = "ssh-key"

  subnet_id = aws_subnet.prometheus-public-subnet.id
  security_groups = [aws_security_group.prometheus-security-group.id]

  tags = {
    Name = "prometheus-server"
  }
}

resource "aws_key_pair" "ssh-key" {
  key_name    = "ssh-key"
  public_key  = var.prometheus-ssh-key
}

