resource "aws_vpc" "prometheus-vpc" {
  cidr_block = var.vpc-cidr-block

  tags = {
    Name = "prometheus-vpc"
  }
}

resource "aws_subnet" "prometheus-public-subnet" {
  cidr_block = var.public-subnet-cidr-block
  vpc_id = aws_vpc.prometheus-vpc.id
  availability_zone = var.az-prometheus
  map_public_ip_on_launch = true

  tags = {
    Name = "prometheus-public-subnet"
  }
}

resource "aws_subnet" "prometheus-private-subnet"{
  cidr_block = var.private-subnet-cidr-block
  vpc_id = aws_vpc.prometheus-vpc.id
  availability_zone = var.az-prometheus

  tags = {
    Name = "prometheus-private-subnet"
  }
}

resource "aws_security_group" "prometheus-security-group" {
  vpc_id = aws_vpc.prometheus-vpc.id

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH for VPC"
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP for Prometheus"
    from_port = 9090
    protocol = "tcp"
    to_port = 9090
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "prometheus-sg"
  }
}

resource "aws_internet_gateway" "prometheus-internet-gateway" {
  vpc_id = aws_vpc.prometheus-vpc.id
}

resource "aws_route_table" "prometheus-route-table" {
  vpc_id = aws_vpc.prometheus-vpc.id
  route {
    cidr_block = var.route-table-cidr-block
    gateway_id = aws_internet_gateway.prometheus-internet-gateway.id
  }
}

resource "aws_route_table_association" "prometheus-route-table-association" {
  route_table_id = aws_route_table.prometheus-route-table.id
  subnet_id = aws_subnet.prometheus-public-subnet.id
}
