variable "aws-region" {
    type = string
    default = "eu-west-3"
}

variable "ami-name" {
    type = string
    default = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
}

variable "ami-owner" {
    type = string
    default = "099720109477" 
}

variable "aws-instance-type" {
    type = string
    default = "t3.micro"
}

variable "vpc-cidr-block" {
    type = string
    default = "10.0.0.0/16"
}

variable "public-subnet-cidr-block" {
    type = string
    default = "10.0.1.0/24"
}

variable "private-subnet-cidr-block" {
    type = string
    default = "10.0.2.0/24"
}

variable "az-prometheus" {
    type = string
    default = "eu-west-3a"
}

variable "route-table-cidr-block" {
    type = string
    default = "0.0.0.0/0"
}

variable "prometheus-ssh-key" {
    type = string
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsrmAeUkmuiYNlet4ic+CuHmdhbf++pHdOmb9Uv/3DCp/xAoDiaKEsYzyOjmq3G3IRq3zkfJO+qMolZbnYMxKBA0JnoU/j+YSuTECF0dI7pYCOjFGK1+G0U5FMPNKpxkifRS8p9EwG7M4AlCF6HOf65A54HUy1bNp35HB2Ozb91o/jlCgruJpF+lXS2gcP73cvr1U6VCG3GW5vHZleIiVaObof8G6eRS3H0DPI4/kvLVArSgmrPPvws+vpMZE/oBEdW7e+yjwvO9tGM9ufgHtMh0Kp1rgv36F7S4IKdIgNUULOk8dGu04eeFTbFqMuN95hm9clXbw3mlo8dmg7nBmh"
}