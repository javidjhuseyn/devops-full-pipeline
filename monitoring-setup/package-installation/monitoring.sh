#!/bin/bash

#update aws cli ans kubectl configuration, otherwise there can be error when trying to use them together
pip3 install awscli --upgrade --user
aws configure
aws eks update-kubeconfig --region eu-west-2  --name eks-cluster

#install Kubernetes metrics server
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

#create prometheus namespace and set it up
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
kubectl create namespace prometheus
helm install prometheus stable/prometheus \
    --namespace prometheus \
    --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2"
kubectl -n prometheus  port-forward deploy/prometheus-server  9090:9090

#create grafana namespace and set it up on top of prometheus after creating prometheus-datasource.yaml file
kubectl create namespace grafana
helm install grafana grafana/grafana \
    --namespace grafana \
    --set persistence.storageClassName="gp2" \
    --set persistence.enabled=true \
    --set adminPassword='He!!oThere' \
    --values prometheus-datasource.yaml \
    --set service.type=LoadBalancer 
#use to get elb address
kubectl get service -n grafana
#use to get password for the admin user
kubectl get secret --namespace grafana grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

