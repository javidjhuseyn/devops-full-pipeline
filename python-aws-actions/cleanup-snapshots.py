import boto3 as bt
from operator import itemgetter
import schedule as sch

ec2_client_paris = bt.client('ec2', region_name='eu-west-3')

# all snapshots we created - self
all_snapshots = ec2_client_paris.describe_snapshots(
    OwnerIds=['self']
).get('Snapshots')

# print(all_snapshots)
# sort the list of dictionaries, recent first
sorted_by_start_time = sorted(all_snapshots, key=itemgetter('StartTime'), reverse=True)

# if we want to cleanup based on volumes, this is the way to go
# prod_volumes = ec2_client_paris.describe_volumes(
#         # Filters=[
#         #     {
#         #         'Name': 'tag:Environment',
#         #         'Values': ['PROD']
#         #     }
#         # ]
#     ).get('Volumes')


def cleanup_snapshots():
    # leave first 2 out, we need to keep them
    for snapshot in sorted_by_start_time[2:]:
        # print(snapshot.get('StartTime'))
        snapshot_id = snapshot.get('SnapshotId')
        delete_snapshot = ec2_client_paris.delete_snapshot(
            SnapshotId=snapshot_id,
        )
        print(f"Snapshot {snapshot_id} is deleted")


sch.every().day.do(cleanup_snapshots())

# keep looping
while True:
    sch.run_pending()


