import boto3 as bt
from operator import itemgetter

ec2_client_paris = bt.client('ec2', region_name='eu-west-3')
ec2_resource_paris = bt.resource('ec2', region_name='eu-west-3')

prod_instance_id = 'i-09fbb492665ee01c2'

prod_volumes = ec2_client_paris.describe_volumes(
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [prod_instance_id]
        }
        ]
).get('Volumes')

# print(prod_volumes)

instance_volume = prod_volumes[0]
# print(instance_volume)

prod_snapshots = ec2_client_paris.describe_snapshots(
    OwnerIds=['self'],
    Filters=[
        {
            'Name': 'volume-id',
            'Values': [instance_volume.get('VolumeId')]
        }
        ]
).get('Snapshots')

latest_snapshot = sorted(prod_snapshots, key=itemgetter('StartTime'), reverse=True)[0]

snapshot_id = latest_snapshot.get('SnapshotId')

new_prod_volume = ec2_client_paris.create_volume(
    AvailabilityZone='eu-west-3b',
    SnapshotId=snapshot_id,
    TagSpecifications=[
        {
            'ResourceType': 'volume',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'PROD'
                }
            ]
        }
    ]
)

while True:
    vol = ec2_resource_paris.Volume(new_prod_volume.get('VolumeId')).state
    print(vol)
    if vol == 'available':
        ec2_resource_paris.Instance(prod_instance_id).attach_volume(
            VolumeId=new_prod_volume.get('VolumeId'),
            Device='/dev/xvdb'
        )
        break

