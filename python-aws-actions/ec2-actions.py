import boto3 as bt

ec2_client_london = bt.client('ec2', region_name='eu-west-2')
# ec2_client_paris = bt.client('ec2', region_name='eu-west-3')

ec2_resource_london = bt.resource('ec2', region_name='eu-west-2')

all_instances_london = ec2_client_london.describe_instances().get('Reservations')

# print(all_instances_london)

ins_id_list_london = []
for data in all_instances_london:
    instances = data.get('Instances')
    # print(instances)
    for ins in instances:
        ins_id = ins.get('InstanceId')
        ins_id_list_london.append(ins_id)

# print(ins_id_list_london)
# adding tags
response = ec2_resource_london.create_tags(
    Resources=ins_id_list_london,
    Tags=[
        {
            'Key': 'Environment',
            'Value': 'DEV'
        },
    ]
)

