import boto3 as bt
# import schedule as sch

ec2_client_paris = bt.client('ec2', region_name='eu-west-3')
# ec2_resource_london = bt.resource('ec2', region_name='eu-west-3')


def create_snapshots():
    all_volumes = ec2_client_paris.describe_volumes(
        # Filters=[
        #     {
        #         'Name': 'tag:Environment',
        #         'Values': ['PROD']
        #     }
        # ]
    ).get('Volumes')
    # print(all_volumes)
    for volume in all_volumes:
        attachments = volume.get('Attachments')
        # print(volume)

        for attachment in attachments:
            volume_id = attachment.get('VolumeId')
            instance_id = attachment.get('InstanceId')
            print(f"Volume ID of Instance - {instance_id} is {volume_id}")

            new_snapshots = ec2_client_paris.create_snapshot(
                VolumeId=volume_id,
            )

            snapshot_id = new_snapshots.get('SnapshotId')
            print(f"New snapshot - {snapshot_id} is created for volume - {volume_id}")


create_snapshots()

# sch.every().day.do(create_snapshots)
# sch.every(30).seconds.do(create_snapshots)

# while True:
#     sch.run_pending()
